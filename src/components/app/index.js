import { BrowserRouter } from "react-router-dom";
import { Route,  Routes } from "react-router";
import Navigation from "../navigation/navigation";
import MainPage from "../../pages/main-page";
import MyProjects from "../../pages/my-projects/my-projects";
import Contacts from "../../pages/contacts";
import About from "../../pages/about";
import Footer from "../footer/footer";
import DetailProject from "../../pages/detail-project/detail-project";
import Resume from "../../pages/resume";
import { useState, useEffect } from "react";


export default function App(){
    const [screenWidth, setScreenWidth] = useState([])
    useEffect(() => {
        setScreenWidth((screenWidth) => {
            screenWidth = window.innerWidth;
        })
    }, [])

    return (
        <>
            <div className="container">     
                <BrowserRouter>   
                    <Navigation></Navigation> 
                    <Routes>
                        <Route path="/" element={<MainPage></MainPage>}></Route>
                        <Route path="/about" element={<About></About> }></Route>
                        <Route path="/projects" element={<MyProjects screenWidth={window.innerWidth} displayName="Projects"></MyProjects>}></Route>
                        <Route path="/project/:id" element={<DetailProject></DetailProject>}></Route>
                        <Route path="/contacts" element={<Contacts displayName="Contacts"></Contacts>}></Route>
                        <Route path='https://www./linkedin.com/in/halyna-synhaivska/' component={() => { window.location.href = 'https://www/linkedin.com/in/halyna-synhaivska/' }}/>
                        <Route path='/resume'  element={<Resume></Resume>}></Route> 
                    </Routes>
                    <Footer></Footer>
                </BrowserRouter>        
            </div>
       </>
    )
}

