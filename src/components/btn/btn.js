import { Link } from "react-router-dom"; 

export default function Btn({className, link, target, title, src}){
   
    return(
        <>
        { 
            title.includes("GitLab") ? <Link to={link} target={target} rel="noreferrer noopener"> 
                <div className={className}> 
                <img src={src} alt={title}></img>{title}</div> </Link>  :
                <Link to={link} target={target} rel="noreferrer noopener">
                    <div className={className}>{title}</div> </Link>                    
        }
        </>
    )
}