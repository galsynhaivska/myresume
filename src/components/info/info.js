import Btn from "../btn/";



export default function Info({className, position, title, txt, titleBtn}){
   
    return(
        <>
            <div className={className}>

                <h1 className="title">{title}</h1>
                <h2 className="work-title">{position}</h2>
                <div className="about">{txt}</div>
                <div className="link-container"> 
                    <Btn className="btn btn-color" title="Projects" link="/projects" target="_self" ></Btn>
                    { 
                    titleBtn.includes("LinkedIn") ? 
                        <Btn className="btn btn-white" title={titleBtn} 
                        link="https://www.linkedin.com/in/halyna-synhaivska"
                        target="_blank"></Btn> : 
                        <Btn className="btn btn-white" title={titleBtn} 
                        link="/resume"
                        target="_blank"></Btn> 
                    }  
                </div>
            </div>
        </>
    
    )
} 