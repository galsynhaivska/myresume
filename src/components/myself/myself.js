export default function Myself({className, src, alt}){
    return(
        <img className={className} src={src} alt={alt}></img>
    )
}