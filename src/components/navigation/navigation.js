import NavItem from "./nav-item";
import { NavLink } from "react-router-dom";
import { useState } from "react";
import navmobile from "../../images/mob-menu.svg";

import "./navigation.css";

export default function Navigation({click}){

const activeLink = 'nav-item-active';
const normalLink = 'nav-item';

const [nav, setNav] = useState({status: "nav-menu"});

function clickMenu(ev){
   
    if(nav.status === "nav-menu"){
        setNav({status: "nav-menu open"})
    }else{
        setNav({status: "nav-menu"})
    }
}

function clickItem(){
    setNav({status: "nav-menu"})
}

    return (
        <>
          <div className="header">

              <div className={nav.status}> 
                <div className="logo"> 
                    <NavLink to={'/'} className={({isActive}) => isActive ? activeLink : normalLink}>
                        <NavItem   title="Halyna Synhaivska"></NavItem> 
                    </NavLink> 
                </div>                        
                <ul className="nav-list">
                    <NavLink to={'/about'} onClick={clickItem} 
                        className={({isActive}) => isActive ? activeLink : normalLink}>
                        <NavItem  title="About" ></NavItem> 
                    </NavLink> 
                    <NavLink to={'/projects'} onClick={clickItem} 
                        className={({isActive}) => isActive ? activeLink : normalLink}>
                        <NavItem title="Projects" ></NavItem>
                    </NavLink>              
                    <NavLink to={'/contacts'} onClick={clickItem} 
                        className={({isActive}) => isActive ? activeLink : normalLink}>
                        <NavItem title="Contacts" ></NavItem>
                    </NavLink>    
                </ul>      
                <div className="nav-menu-mobile">
                    <img className="nav-button" onClick={clickMenu}
                    src={navmobile} alt="navigation-button"></img>
                </div>
            </div>
        </div>
        </>
    )
}