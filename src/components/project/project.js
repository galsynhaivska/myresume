import { Link } from "react-router-dom";
import Myself from "../myself/myself";
import Title from "../title";

export default function Project({title, img, index, className}){
    return(  
        <div className={className}>  
        <Link to={`/project/${index}`}>
            <div className="project-photo">
                <Myself className="photo" src={img} alt={title}></Myself>
            </div>   
            <Title className="title-project" title={title}></Title>   
        </Link>  
        </div>
        
    )
}
