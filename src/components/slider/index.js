import SlideBtn from "./slide-btn";
import { useState, useEffect, Children, cloneElement } from "react";


import "./slider.css";


let PROJECT_WIDTH 

const Slider = ({children, screenWidth }) => {
        if( screenWidth > 1200 ) {  
            PROJECT_WIDTH = 900
        }else if( screenWidth  > 992 ) {
            PROJECT_WIDTH =750
        }else if( screenWidth  > 768 ) {
            PROJECT_WIDTH = 575
        }else if( screenWidth  > 576 ) {  
            PROJECT_WIDTH = 450
        }else if( screenWidth  >= 450 ) {
            PROJECT_WIDTH = 350 
        }else{
            PROJECT_WIDTH = 300
        } 

    const [projects, setProjects] = useState([]);
    const [offset, setOffset] = useState([0])

    const LeftBtnClick = () =>{
        setOffset((offset) => {
            return Math.min(offset + PROJECT_WIDTH, 0)
        })
    }
    const RightBtnClick = () =>{
        setOffset((offset) => {
            return  Math.max(offset - PROJECT_WIDTH, -PROJECT_WIDTH * (projects.length - 1))
        })
    }

    useEffect(() => {
        setProjects(
            Children.map(children, child => {
                
                return cloneElement(child, {                
                    style: {  
                        minWidth: `${PROJECT_WIDTH}px`,
                        maxWidth: `${PROJECT_WIDTH}px`,
                    }
                })
            })
        )
    }, [])

   

    return(
        <>
        <SlideBtn className="slide-btn" value="<" click={LeftBtnClick}></SlideBtn>
        <div className="slider-container">
          
            <div className="slide-window">
                <div className="all-projects"
                style={{
                    transform: `translateX(${offset}px)`,
                }}
                >
                    {projects}
                </div>
            </div>

        </div>
        <SlideBtn className="slide-btn" value=">" click={RightBtnClick}></SlideBtn>
        </>
    )
}

export  default  Slider 