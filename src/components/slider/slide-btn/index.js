
const SlideBtn = ({className, value, click}) => {
    return(
        <div className={className} onClick={click}>{value}</div>
    )
}
export default SlideBtn