
export default function Title({className, title}){
    return(
        <h3 className={className}>{title}</h3>
    )
}