import myselfi01 from "../images/myselfi.png";

 const myData = {
    position: "Frontend developer",
    myName: "Halyna Synhaivska",
    infoAbout: `I'm  Halyna Synhaivska. I'd like to give my experience in real projects. 
    I'm interested in working with projects that need to use numerical methods 
    and/or apply my knowledge from  statistics, mathematics, physics`,
    infoMain: ``,
    resume: "./resume.pdf",
    selfi: myselfi01,
    me: myselfi01
}

export {myData}