import project01 from "../images/project01.png";
import project02 from "../images/project02.png";


const dataProjects = [
    {
        id: "1",
        title: "MissCupcake",
        skills: "HTML, CSS, JS",
        img: project02,
        imgBig: project02,
        gitHubLink: 'https://gitlab.com/galsynhaivska/misscapcake.git',
        description:`This project was created during my training in the 7education frontend 
        course. The goal of the project is to create a website for a cafe `    
    },
    {
        id: "2",
        title: "Space",
        skills: "HTML, CSS, JS, React",
        img: project01,
        imgBig: project01,
        gitHubLink: 'https://gitlab.com/galsynhaivska/planet.git',
        description:`This project was created during my training in the 7education frontend 
        course. The goal of the project is to create a website with animation `    
    },
    ];
export {dataProjects};