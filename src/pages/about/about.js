import {myData} from "../../data/my-data";
import Myself from "../../components/myself/myself";
import Info from "../../components/info/info";


export default function About(){
    
    return(
        <>
        <main className="main-container">  
            <div className="intro-info about-info">
                <div className="foto-container">
                    <Myself className="selfi" src={myData.selfi} alt={myData.myName}></Myself>
                </div>          
                <Info  className="info-container" 
                    screen="about"
                    position={myData.position} 
                    title={myData.myName} 
                    txt={myData.infoAbout}
                    titleBtn="Resume">      
                </Info>   
            </div>          
        </main>
    
        </>
    )
}