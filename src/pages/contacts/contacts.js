import { Link } from "react-router-dom";

import "./contacts.css";

export default function Contacts({displayName}){
    return(
        <>
        <div className="contact-container">
        <h1 className="title detail-title">{displayName}</h1>
          <div className="contacts">
              <h2 className="title-contacts"> E-mail:</h2>
              <Link to="mailto:galsynhaivska@gmail.com">
              <p className="work-title grey"> galsynhaivska@gmail.com</p>
              </Link>
             
              <h2 className="title-contacts">LinkedIn</h2>
              <Link to="https://www.linkedin.com/in/halyna-synhaivska" 
                target="_blank" rel="noreferrer noopener">
                <p className="work-title grey"> linkedin.com/in/halyna-synhaivska</p>
              </Link>
          </div>
        </div>
       
        </>
    )
}