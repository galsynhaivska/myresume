import { useParams } from "react-router-dom";

import Title from "../../components/title";
import Btn from "../../components/btn/btn";

import { dataProjects } from "../../data/project-data";
import gitlabIcon from "../../images/gitlab-icon.png";

import "./detail-project.css";

const DetailProject = () => {
    const {id} = useParams();
    const idProject = dataProjects[id];
    return ( 
        <>
            <div className="main-container">
                <Title className="title detail-title" title={idProject.title}></Title> 
                <div className="project-info">
                    <div className="detail-photo">
                        <img className="photo det-photo" src={idProject.imgBig} alt={dataProjects.title}></img>  
                    </div>
                    <div className="about details-about">{idProject.description}</div>
                    <div className="about details-about">Skills: {idProject.skills}</div>
                    <div className="link-container my-project-page"> 
                        <Btn className="btn btn-color" title="All projects" link="/projects" target="_self" ></Btn>
                        {idProject.gitHubLink && (
                            <Btn className="btn btn-white btn-git" title="GitLab" src={gitlabIcon}
                                link={idProject.gitHubLink}
                                target="_blank">
                            </Btn>
                        )}  
                    </div>    
                </div> 
            </div>
        </>
     );
}
 
export default DetailProject;