import Info from "../../components/info";
import Myself from "../../components/myself";
import { myData } from "../../data/my-data";


import "./main-page.css";




export default function MainPage(){

    return(
        <>
        <main className="main-container">  
            <div className="intro-info">
                <div className="foto-container">
                    <Myself className="selfi" src={myData.selfi} alt={myData.myName}></Myself>
                </div>          
                <Info  className="info-container" 
                    screen="main-page"
                    position={myData.position} 
                    title={myData.myName} 
                    txt={myData.infoMain}
                    titleBtn="LinkedIn">
                </Info>  
            </div>          
        </main>
        </>
    )
}