import Project from "../../components/project";
import Slider from "../../components/slider";
import { dataProjects } from "../../data/project-data";


import "./my-projects.css";


export default function MyProjects({displayName, screenWidth}){
    return(
        <main className="main-container">  
            <h1 className="title projects-title">{displayName}</h1> 
            <div className="projects-slider-container"> 
                <Slider screenWidth={screenWidth}>
                {
                        dataProjects.map((proj, index) => {
                            return <Project className="item-project"
                                key={proj.id}
                                title={proj.title} 
                                img={proj.img}
                                index={index}></Project>
                        })                    
                    }            
                </Slider> 
            </div>
            {

          /*
            <div className="intro-info"> 
                {
                    dataProjects.map((proj, index) => {
                        return <Project className="projects-container"
                            key={proj.id}
                            title={proj.title} 
                            img={proj.img}
                            index={index}></Project>
                    })                    
                }            
            </div>   
            */ }
        </main>   
    )
}