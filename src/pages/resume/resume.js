import { Link } from "react-router-dom";
import resume from "../../images/resume_Synhaivska-2023.jpg";
import "./resume.css";

export default function Resume({link}){
    return (
        <>
        <Link to={link}>
            <img className="resume" src={resume} alt="resume"></img>
        </Link>
        </>
    )
}